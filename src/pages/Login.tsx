import React, { useState, useEffect } from 'react';
import { Alert, Box, Button, TextField } from '@mui/material';
import { selectUser, User, login } from '../userReducer';
import { useAppDispatch, useAppSelector } from '../hooks';
import { Link, useNavigate } from "react-router-dom";
import FormContaner from '../FormContaner';



export default function LoginPage() {

    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const user = useAppSelector(selectUser);

    const [inputName, setInputName] = useState('');
    const [inputPassword, setInputPassword] = useState('');
    const [doRegister, setDoRegister] = useState(false);

    useEffect(() => {
        if (user !== null) {
            navigate("/");
        }
    }, [user?.name, navigate]);


    const setName = (text: string) => {
        setInputName(text);
        setDoRegister(false);
    }
    const setPassword = (text: string) => {
        setInputPassword(text);
        setDoRegister(false);
    }

    const submitForm = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const payload: User = { name: inputName, password: inputPassword };
        dispatch(login(payload));
        setDoRegister(true);
    }

    return (
        <>
            <FormContaner name='Вход' >
                <Box
                    component="form"
                    onSubmit={submitForm}
                    sx={{ mt: 1 }}
                >
					 <TextField
						margin="normal"
						fullWidth
						required
						type='text'
						label="Имя пользователя"
						onChange={(e) => { setName(e.target.value); }}
						value={inputName}
						autoComplete='off'
					/>

					<TextField
						margin="normal"
						fullWidth
						required
						type='password'
						label="Пароль"
						onChange={(e) => { setPassword(e.target.value); }}
						value={inputPassword}
						autoComplete='off'
					/>

					{(doRegister) &&
						<Alert variant="outlined" severity="error">
                        	Неправильно заданы имя пользователя или пароль.
                    	</Alert>
					}
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        size="large"
                        sx={{ mt: 2, mb: 2 }}
                    >
                        Войти
                    </Button>

                    Нет пользователя? <Link to="/register" >
                        Зарегистрироваться
                    </Link>
                </Box>
            </FormContaner>
        </>
    );
}