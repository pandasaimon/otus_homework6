import { Box, Button, Typography } from '@mui/material';
import { useAppSelector, useAppDispatch } from '../hooks';
import { logout, selectUser } from '../userReducer';
import { useNavigate } from 'react-router-dom';


export default function HomePage() {
    const user = useAppSelector(selectUser);
    const dispatch = useAppDispatch();
    const navigate = useNavigate();

    const logoutClick = () => {
        dispatch(logout());
    };
    
    return (
        <>
            <Box sx={{
                marginTop: 8,
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
            }}>

                <main>
                    <Typography
                        component="h1"
                        variant="h3"
                        align="center"
                        color="text.primary"
                        gutterBottom
                    >
                        Добро пожаловать{((user !== null) && <>, {user.name}!</>) || <>!</>}
                    </Typography>
					{(user === null) && 
						<Box sx={{ flexDirection: 'row', display: 'flex', justifyContent: 'center' }}>
                            <Button variant="contained" onClick={() => { navigate("/login"); }} sx={{ width: 2 / 4, mr: 2 }} >Войти</Button>
                            <Button variant="contained" onClick={() => { navigate("/register"); }} sx={{ width: 2 / 4, mr: 2 }}  >Зарегистрироваться</Button>
                        </Box>
					}

                    {user !== null &&
						<>
							<Box sx={{ flexDirection: 'row', display: 'flex', justifyContent: 'center' }}>
								<Typography
									component="h3"
									variant="h5"
									align="center"
									color="text.primary"
									gutterBottom
								>
									Вы успешно авторизовались, приятного пользования!
								</Typography>
							</Box>
							<Box sx={{ flexDirection: 'row', display: 'flex', justifyContent: 'center' }}>
								<Button variant="contained" onClick={logoutClick} sx={{ width: 1 / 4, mr: 2 }} >Выйти</Button>
							</Box>
						</>
                    }
					
                </main>
            </Box>
        </>
    );
}