import React, { useState, useEffect } from 'react';
import { Box, Button, TextField, Typography } from '@mui/material';
import { Link, useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from '../hooks';
import { register, logout, selectCreateNewUser, selectUserActionError } from '../userReducer';
import FormContaner from '../FormContaner';

export default function Register() {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const actionError = useAppSelector(selectUserActionError);
    const userStatus = useAppSelector(selectCreateNewUser);

    const [password2Error, setPassword2Error] = useState(false);
    const [userNameError, setUserNameError] = useState(false);
    const [doRegister, setDoRegister] = useState(false);

    const [inputName, setInputName] = useState('');
    const [inputPassword, setInputPassword] = useState('');
    const [inputPassword2, setInputPassword2] = useState('');


    useEffect(() => {
        if (userStatus === true) {
            setDoRegister(true);
            dispatch(logout());
        }
        if (actionError !== '') {
            setUserNameError(true);
        }
    }, [actionError, userStatus, dispatch]);


    const submitForm = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        if (inputPassword !== inputPassword2) {
            setPassword2Error(true);
        }
        else {
            dispatch(register({ name: inputName, password: inputPassword }));
        }
    }

    const setName = (text: string) => {
        setInputName(text);
        setUserNameError(false);
    }
    const setPassword = (text: string) => {
        setInputPassword(text);
        setPassword2Error(false);
    }

    const setPassword2 = (text: string) => {
        setInputPassword2(text);
        setPassword2Error(false);
    }

    return (
        <>
            <FormContaner name='Регистрация' >

                {!doRegister &&
                    <Box
                        component="form"
                        onSubmit={submitForm}
                        sx={{ mt: 1 }}
                    >

						<TextField
							margin="normal"
							fullWidth
							required
							error={userNameError}
							type="text"
							label="Имя пользователя"
							helperText={userNameError? "Такой пользователь уже существует" : null}
							onChange={(e) => { setName(e.target.value); }}
							value={inputName}
							autoComplete='off'
						/>

						<TextField
							margin="normal"
							fullWidth
							required
							type="password"
							label="Пароль"
							onChange={(e) => { setPassword(e.target.value); }}
							value={inputPassword}
							autoComplete='off'
						/>

						<TextField
							margin="normal"
							fullWidth
							required
							type="password"
							label="Повторите пароль"
							error={password2Error}
							helperText={password2Error? "Пароли не совпадают" : null}
							onChange={(e) => { setPassword2(e.target.value); }}
							value={inputPassword2}
							autoComplete='off'
						/>
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            size="large"
                            sx={{ mt: 2, mb: 2 }}
                        >
                            Зарегистрироваться
                        </Button>
                        Уже есть пользователь? <Link to="/login"  >Войти</Link>
                    </Box>
                }
                {doRegister === true &&
                    <>
                        <Typography variant="h6" sx={{ mt: 2 }} align="center" color="text.secondary" paragraph>
                            Вы успешно зарегистированы!
                        </Typography>
                        <Button type="submit"
                            fullWidth
                            variant="contained"
                            size="large"
                            sx={{ mt: 2, mb: 2 }}
                            onClick={() => { navigate("/login"); }}
                        >Вход</Button>

                    </>
                }
            </FormContaner>
        </>
    );
}
