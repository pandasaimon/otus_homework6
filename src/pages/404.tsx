const Page404 = () => {

    const css = {
        margin: '10px',
        padding:'10px',
    };

    return <div style={css}><h1>404 Страница не найдена</h1></div>
};

export default Page404;