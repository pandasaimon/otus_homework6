import {
  createBrowserRouter,
  RouterProvider,
  createRoutesFromElements,
  Route,
} from "react-router-dom";
import './App.css';
import HomePage from './pages/Home';
import LoginPage from './pages/Login';
import Register from './pages/Registration';
import Page404 from './pages/404';
import Root from './pages/Root';
import { createTheme, ThemeProvider } from '@mui/material/styles';

function App() {
  const router = createBrowserRouter(
    createRoutesFromElements(
      <Route
        path="/"
        element={<Root />}
        errorElement={<Page404 />}
      >
        <Route index element={<HomePage />} />
        <Route errorElement={<Page404 />}>
          <Route
            path="login"
            element={<LoginPage />}
          />
          <Route
            path="register"
            element={<Register />}
          />
        </Route>
      </Route>
    )
  );

  const theme = createTheme({
    palette: {
        primary: {
            main: '#3baa58'
        },
        secondary: {
            main: '#aa3b8e'
        },
    },
});

  return (
    <ThemeProvider theme={theme}>
      <RouterProvider router={router} />
    </ThemeProvider>
  );
}

export default App;
