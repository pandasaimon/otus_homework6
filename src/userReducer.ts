import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from './store';

export interface User {
    name: string;
    password: string;
}

export interface UserState {
    user: User| null;
    error: string;
    createNewUser:Boolean;
    users: Array<User>;
}



const initialState: UserState = {
    user: null,
    error: '',
    createNewUser: false,
    users: [{ name: 'test', password: 'test' }, { name: 'bob', password: 'bob' }, { name: 'alice', password: 'alice' }]
}


export const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        login: (state, action: PayloadAction<User>) => {
            console.log('login');
            if (state.users.findIndex(x => (x.name === action.payload.name) && (x.password === action.payload.password)) !== -1) {
                state.user = action.payload;
                state.error = '';
                state.createNewUser=false;
                
            }
            else {
                state.error = 'Неправильные имя пользователя или пароль';
                state.user = null;
                state.createNewUser=false;
            }
        },

        logout: (state) => {
            state.createNewUser=false;
            state.error = '';
            state.user = null;
        },
       
        register: (state, action: PayloadAction<User>) => {
            if (state.users.findIndex(x => (x.name === action.payload.name)) !== -1) {
                state.error = 'Пользователь с таким именем уже зарегистрирован';
                state.createNewUser=false;
            }
            else {
                state.error = '';
                state.users.push(action.payload);
                state.createNewUser=true;
            }
        },
    }
});

export const { login, logout, register } = userSlice.actions;
export const selectUser = (state: RootState) => state.user.user;
export const selectUserActionError = (state: RootState) => state.user.error;
export const selectCreateNewUser = (state: RootState) => state.user.createNewUser;
export default userSlice.reducer;