import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import userReducer from './userReducer';

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;

const store = configureStore({
    reducer: {
        // Регистрируем два редюсера
        // Под выбранного пользователя
        user: userReducer,
    },

});
export default store;